Laybuy OpenCart Extension
https://www.laybuy.com/

Developed by: Wild Software Ltd / Chreos
Developer Website: www.chreos.com
Copyright: Wild Software Ltd.
Date: 2018-11-2

## Overview

The LayBuy OpenCart Extension allows merchants to receive payments in their OpenCart website through via the Laybuy hosted payment solution.
LayBuy allows you to offer a simpler way for shoppers to pay in 6 weekly interest free instalments while receiving your payment immediately.
OpenCart is a free open source ecommerce platform for online merchants.

Laybuy is available in New Zealand, Australia and the United Kingdom.

The formula used to determine repayments:
For NZD & AUD Currency:
If price <= $1,200 then 6 weekly interest free payments from $[price / 6]
If price >  from $1,200 then $[price - 1000] today & 5 weekly interest free payments from $200

For GBP Currency:
If price <= £600 then 6 weekly interest free payments from £[price / 6]
If price > £600 then from £[price - 500] today & 5 weekly interest free payments from £100

Note: Currency applied at checkout and for the limits is based on your stores currency. Payment by LayBuy will be taken in the currency of your store if available. 


## Laybuy Installation:

1. Backup your OpenCart installation, just to be safe.

2. Use the OpenCart "Extension Installer" in your websites OpenCart admin panel ([Your URL]/admin/index.php?route=extension/installer).
   Detailed instructions can be found here: http://docs.opencart.com/en-gb/extension/installer/
OR
   Copy the folders contained in the upload/ folder in this .zip archive into your website root directory matching the same structure.

2. In your website OpenCart admin panel and go to Payment Extenstions and click the edit button for the Laybuy extension.
   Detailed instructions about payment gateways in OpenCart can be found here: http://docs.opencart.com/en-gb/extension/payment/
  
3. Configure the LayBuy payment gateway with the required settings. You can hover ? where displayed next to a setting if you need help.

4. Test the payment method is set up correctly and your logs do not show any errors.

5. Set up this URL (https://yoursite.com/index.php?route=payment/laybuy_gateway/cron) as a cron or scheduled task to run once every hour 
   e.g. * * * * * wget "https://yoursite.com/index.php?route=payment/laybuy_gateway/cron"

Note: The Laybuy price breakdown info will only show on the product info page if VQMOD is installed exclude the script if you do not wish to show price breakdowns

Category Price breakdown: The price breakdown on the category page can be disabled or added manually to your template. Please view the comments in the file vqmod/xml/laybuy.xml within this extension for instructions on how.
  
  

## Laybuy Upgrade:
IMPORTANT: Files and database backup is highly recommended.

First, follow the same steps as in the Installation Instructions above.

Finally, go to Extensions > Payment > Laybuy Gateway. Review your settings and click "Save".


That's all!  

## Links

- LayBuy homepage: https://www.laybuy.com/
- Developers homepage (Chreos):(https://www.chreos.com/)
- OpenCart homepage: https://www.opencart.com/
- OpenCart documentation: https://docs.opencart.com/


## Help or reporting a bug

Contact Laybuy at https://www.laybuy.com/nz/contact if you need assistance.

Read the instructions below before you report a bug.

 1. If your bug is related to the OpenCart core code then please review their process for reporting issues
 2. Make sure you have the latest and up to date version of the module and it is compatible with your version of OpenCart
 3. Make sure that your bug/issue is not related to your customisations or hosting environment
 4. Try Google to see if you can find a solution to your issue

If you are not sure about your issue, it may be a good idea to ask the community like (http://forum.opencart.com/viewforum.php)



## License

This extension project is developed and licensed by Chreos for use on the understanding it will be used for the sole purpose mentioned above and not redistributed, modified or copied in anyway without explicit written approval from Chreos.

This extension is provided for the default installation of OpenCart. No guarantee is provided that it will work with other customisations you've made.


DISCLAIMER:
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

IF YOU DO NOT AGREE TO THIS LICENSE & DISCLAIMER YOU MAY NOT USE THIS SOFTWARE