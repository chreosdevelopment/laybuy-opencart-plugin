<?php

class Laybuy extends Controller {
  var $merchant_id = '';
  var $api_key = '';
  var $url = 'https://api.laybuy.com/';
  var $sb = 'https://sandbox-api.laybuy.com/';
  var $debug = false;

  public function __construct($registry) {
    parent::__construct($registry);

    $this->merchant_id = $this->config->get('laybuy_gateway_merchant_id');
    $this->api_key = $this->config->get('laybuy_gateway_api_key');
    $this->url = $this->config->get('laybuy_gateway_mode')? $this->url : $this->sb;                
    $this->debug = $this->config->get('laybuy_gateway_debug');
  }


  public function confirmOrder($token) {
    $this->log(__METHOD__, "Creating Laybuy call to confirm order.");

    $json = json_encode(array("token" => $token));

    $response = $this->api($this->getUrl('order/confirm'), $json);

    if ((empty($response['result']) || $response['result'] != 'SUCCESS')){
      $this->log(__METHOD__, "Failed to confirm Labuy order" . (!empty($response['error'])? " ({$response['error']})" : " (No response data)"));
    }

    return !empty($response['result'])? $response : false;
  }

  public function createOrder($order_data) {
    $this->log(__METHOD__, "Creating Laybuy call to create order.");

    $json = json_encode($order_data);
    
    //DEBUG
    //$this->log(__METHOD__, $json);

    $response = $this->api($this->getUrl('order/create'), $json);

    if ((empty($response['token']) || $response['result'] != 'SUCCESS')){
      $this->log(__METHOD__, "Failed to create Labuy order" . (!empty($response['error'])? " ({$response['error']})" : " (No response data)"));
    }

    return !empty($response['result'])? $response : false;
  }
    

  function getURL($endpoint){
    return $this->url.$endpoint;    
  }  
  
  /*
  * Makes the curl call to the laybuy web server
  */
  function api($url, $json){
    $ch = curl_init();
    
    if (empty($url) || empty($this->merchant_id) || empty($this->api_key)) {
      
      $this->log(__METHOD__, "ERROR! One or more API settings are not configured");
      return false;
    }
    
    $this->log(__METHOD__, "Making CURL call to $url");
    
    curl_setopt($ch, CURLOPT_HEADER, 0); 
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8'));
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_USERPWD, "{$this->merchant_id}:{$this->api_key}");
    curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
    //curl_setopt($ch, CURLOPT_VERBOSE, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);


    $curl_result = curl_exec ($ch); // Open connection
    $curl_error = curl_error($ch);
    curl_close($ch); // Close the connection  
    
    
    if(!empty($curl_error)){ 
      $this->log(__METHOD__, "CURL ERROR - $curl_error");
      return false;
    } else {
      return json_decode($curl_result, true);
    }
  }   
  
  
  function log($mth, $message){
    if ($this->debug != false){
      $this->log->write("DEBUG {$mth}:$message");
    }
  }


 
}
?>
