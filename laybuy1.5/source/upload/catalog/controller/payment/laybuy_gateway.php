<?php
class ControllerPaymentLaybuyGateway extends Controller {

    var $aud_limit      = 1440;
    var $nzd_limit      = 1440;
    var $default_limit  = 720;

    public function __construct($registry) {
      parent::__construct($registry);
      
      require_once(DIR_SYSTEM . 'library/laybuy.php');
      $this->laybuy = new Laybuy($this->registry);   
    }


    public function index() {
			$this->load->language('payment/laybuy_gateway');

			$data = array();
			$this->data['text_heading'] = $this->language->get('text_heading');
			$this->data['text_instruction'] = $this->language->get('text_instruction');
			$this->data['text_loading'] = $this->language->get('text_loading');
			$this->data['button_confirm'] = $this->language->get('button_confirm');

			//--- formatting for price breakdown
			$order_id = $this->session->data['order_id'];
			$this->load->model('checkout/order');
			$order_info = $this->model_checkout_order->getOrder($order_id);

			//Get limit based on currency or get default
			$var = strtolower($this->config->get('config_currency'))."_limit";
			$limit = isset($this->$var)? $this->$var : $this->default_limit;

			if ($order_info['total'] > $limit) {
					$repayment = floor($limit / 6 * 100)/100;
					$excess = $this->currency->format($order_info['total'] - (5*($limit / 6)), $order_info['currency_code']);
					$repayment = $this->currency->format($repayment, $order_info['currency_code'], $order_info['currency_value']);
					$this->data['text_payment'] = sprintf($this->language->get('text_payment_excess'), $excess, $repayment);
			} else {
					$repayment = $this->currency->format(floor($order_info['total'] / 6 * 100)/100, $order_info['currency_code']);
					$this->data['text_payment'] = sprintf($this->language->get('text_payment'), $repayment);
			}

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/payment/laybuy_gateway.tpl')) {					
				$this->template = $this->config->get('config_template') . '/template/payment/laybuy_gateway.tpl';					
			} else {
				$this->template = 'default/template/payment/laybuy_gateway.tpl';
			}
			$this->render();
    }



    public function send() {
			$this->load->language('payment/laybuy_gateway');
			$result = false;

			if($this->config->get('laybuy_gateway_status')) {
				$this->load->model('checkout/order');
				$this->load->model('account/order');

				$order_id = $this->session->data['order_id'];
				$order_info = $this->model_checkout_order->getOrder($order_id);

				if ($order_info) {
					$data = array(
							"amount" => $order_info['total'],
							"currency" => $order_info['currency_code'],
//          "returnUrl" => $this->url->link('laybuy_gateway', '', true),
							"returnUrl" => $this->url->link('payment/laybuy_gateway/confirm', '', true),
							"merchantReference" => $order_info['order_id']
					);

					$data['customer'] = array(
							"firstName" => $order_info["firstname"],
							"lastName" => $order_info["lastname"],
							"email" => $order_info["email"],
							"phone" => $order_info["telephone"]
					);

					$data['billingAddress'] = array(
							"name" => "{$order_info['payment_firstname']} {$order_info['payment_lastname']}",
							"address1" => $order_info["payment_address_1"],
							"address2" => $order_info["payment_address_2"],
							"city" => $order_info["payment_city"],
							"state" => $order_info["payment_zone"],
							"postcode" => $order_info["payment_postcode"],
							"country" => $order_info["payment_country"],
					);

					$data['shippingAddress'] = array(
							"name" => "{$order_info['shipping_firstname']} {$order_info['shipping_lastname']}",
							"address1" => $order_info["shipping_address_1"],
							"address2" => $order_info["shipping_address_2"],
							"city" => $order_info["shipping_city"],
							"state" => $order_info["shipping_zone"],
							"postcode" => $order_info["shipping_postcode"],
							"country" => $order_info["shipping_country"],
					);

					$products = $this->model_account_order->getOrderProducts($order_id);
					$order_tax = 0;
					$item_total = 0;
					foreach ($products as $product) {
							$item_total += ($product['price'] + $product['tax']) * $product['quantity'];
							$order_tax += $product['tax'] * $product['quantity'];

							$data['items'][] = array(
//									"id" => $product['model'],
									"id" => $product['order_product_id'],
									"description" => $product['name'],
									"quantity" => $product['quantity'],
									"price" => $product['price'] + $product['tax']
							);
					}

					$shipping = isset($this->session->data['shipping_method'])? $this->session->data['shipping_method']['cost'] : 0;
					if (!empty($shipping) && $this->session->data['shipping_method']['tax_class_id']) {
						$tax_rate = $this->tax->calculate($shipping, $this->session->data['shipping_method']['tax_class_id']);

						$order_tax += $tax_rate;
						$shipping += $tax_rate;
					}        

					if (!empty($shipping)) {                  
							$data['items'][] = array(
									"id" => "SHIPPING",
									"description" => $order_info['shipping_method'],
									"quantity" => 1,
									"price" =>  $shipping
							);
							$item_total += $shipping;
					}

					$other = $order_info['total'] - $item_total;

					//Discount on order accounting for rounding adjustment
					if (!empty($other) && $other < -0.01) {
							$data['items'][] = array(
									"id" => "DISCOUNT",
									"description" => $this->language->get('text_discount'),
									"quantity" => 1,
									"price" => $other
							);
					}

					//Other costs and adjustments on order accounting for rounding adjustment
					if (!empty($other) && $other > -0.01) {
						$data['items'][] = array(
								"id" => "OTHER",
								"description" => $this->language->get('text_other_cost'),
								"quantity" => 1,
								"price" => $other   
						); 
					}

					//DEBUG
					//$data['other'] = $other;
					//$data['item_total'] = $item_total;


					//ONLY provide tax if enabled
					if ($this->config->get('laybuy_gateway_tax')) {
							$data['tax'] = $order_tax;
					}

					//DEBUG
					//$this->log->write($data);

					$result = $this->laybuy->createOrder($data);

					//DEBUG
					//$this->log->write($result);
				}
			}


			if($result) {   
				switch ($result['result']) {
					case 'SUCCESS':
						$this->load->model('payment/laybuy_gateway');
						$this->model_payment_laybuy_gateway->setLaybuyToken($order_id, $result['token']);
						echo $result['paymentUrl'];
						break;
					case 'ERROR':
					default:
						$this->session->data['error'] = !empty($result['error'])? $result['error'] : $this->language->get('error_laybuy');          
						echo $this->url->link('checkout/checkout');
						break;
				}
			} else {
				$this->session->data['error'] = $this->language->get('error_laybuy');
				echo $this->url->link('checkout/checkout');
			}
    }


    public function confirm($laybuy_token = false) {
        $result = false;
												
        $token = !empty($this->request->get['token'])? $this->request->get['token'] : $laybuy_token;

        $this->load->model('payment/laybuy_gateway');
        $order_id = $token? $this->model_payment_laybuy_gateway->getOrderId($token) : false;				
				
        if($order_id) {    
            $result =  $this->laybuy->confirmOrder($token);
        }

        if ($result !== false){
            switch ($result['result']) {
                case 'SUCCESS':				
										$this->load->model('checkout/order');										
										$this->model_checkout_order->confirm($order_id, $this->config->get('laybuy_gateway_order_status_id'));								
                    if ($laybuy_token) return 'abandoned transaction found and processed'; //cron call
                    $this->redirect($this->url->link('checkout/success', '', true));
                    break;
                case 'ERROR':
                case 'DECLINED':
                default:
                    $this->session->data['error'] = !empty($result['error'])? $result['error'] : $this->language->get('error_laybuy_confirm'); 
                    if ($laybuy_token) return $this->session->data['error']; //cron call
                    $this->redirect($this->url->link('checkout/checkout', '', true));
                    break;
            }
        } else {
            if ($laybuy_token) return 'connection issue see log'; //cron call
            $this->redirect($this->url->link('checkout/checkout', '', true));
        }
    }


    public function cron() {
        $this->load->model('payment/laybuy_gateway');
        $orders = $this->model_payment_laybuy_gateway->getOrders();

        foreach ($orders as $order) {
            echo "Checking: {$order['laybuy_token']} ---- ";
            $result = $this->confirm($order['laybuy_token']);
            echo "Result: \"{$result}\"<br>";
        }

        echo "COMPLETE";
    }

}
?>
