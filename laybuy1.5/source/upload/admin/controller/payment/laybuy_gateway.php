<?php
class ControllerPaymentLaybuyGateway extends Controller {
    private $error = array();

    public function index() {
      $this->load->language('payment/laybuy_gateway');
      $this->document->setTitle($this->language->get('heading_title'));
      $this->load->model('setting/setting');

      if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
        $this->model_setting_setting->editSetting('laybuy_gateway', $this->request->post);

        $this->session->data['success'] = $this->language->get('text_success');

        $this->redirect($this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL'));
      }
      
			$this->data['heading_title'] = $this->language->get('heading_title');

      $this->data['text_edit'] = $this->language->get('text_edit');
      $this->data['text_enabled'] = $this->language->get('text_enabled');
      $this->data['text_disabled'] = $this->language->get('text_disabled');
      $this->data['text_all_zones'] = $this->language->get('text_all_zones');

      $this->data['text_sandbox'] = $this->language->get('text_sandbox');
      $this->data['text_live'] = $this->language->get('text_live');
      
      $this->data['text_standard'] = $this->language->get('text_standard');
      $this->data['text_non_standard'] = $this->language->get('text_non_standard');

      $this->data['entry_api_key'] = $this->language->get('entry_api_key');
      $this->data['entry_merchant_id'] = $this->language->get('entry_merchant_id');
      $this->data['entry_mode'] = $this->language->get('entry_mode');
      $this->data['entry_debug'] = $this->language->get('entry_debug');
      $this->data['entry_total'] = $this->language->get('entry_total');
      $this->data['entry_max_total'] = $this->language->get('entry_max_total');
      $this->data['entry_order_status'] = $this->language->get('entry_order_status');
      $this->data['entry_total'] = $this->language->get('entry_total');
      $this->data['entry_tax_type'] = $this->language->get('entry_tax_type');
      $this->data['entry_geo_zone'] = $this->language->get('entry_geo_zone');        
      $this->data['entry_status'] = $this->language->get('entry_status');
      $this->data['entry_sort_order'] = $this->language->get('entry_sort_order');

      $this->data['help_total'] = $this->language->get('help_total');
      $this->data['help_max_total'] = $this->language->get('help_max_total');
      $this->data['help_debug'] = $this->language->get('help_debug');
      $this->data['help_tax'] = $this->language->get('help_tax');

      $this->data['button_save'] = $this->language->get('button_save');
      $this->data['button_cancel'] = $this->language->get('button_cancel');

      if (isset($this->error['warning'])) {
          $this->data['error_warning'] = $this->error['warning'];
      } else {
          $this->data['error_warning'] = '';
      }

      $this->data['breadcrumbs'] = array();

      $this->data['breadcrumbs'][] = array(
          'text' => $this->language->get('text_home'),
          'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
      );

        $this->data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_payment'),
            'href' => $this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
        );

      $this->data['breadcrumbs'][] = array(
          'text' => $this->language->get('heading_title'),
          'href' => $this->url->link('payment/laybuy_gateway', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
      );

      $this->data['action'] = $this->url->link('payment/laybuy_gateway', 'token=' . $this->session->data['token'], 'SSL');

      $this->data['cancel'] = $this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL');


      if (isset($this->request->post['laybuy_gateway_mode'])) {
       $this->data['laybuy_gateway_mode'] = $this->request->post['laybuy_gateway_mode'];
      } else {
       $this->data['laybuy_gateway_mode'] = $this->config->get('laybuy_gateway_mode');
      }      
            
      if (isset($this->request->post['laybuy_gateway_api_key'])) {
          $this->data['laybuy_gateway_api_key'] = $this->request->post['laybuy_gateway_api_key'];
      } else {
          $this->data['laybuy_gateway_api_key'] = $this->config->get('laybuy_gateway_api_key');
      }
      
      if (isset($this->request->post['laybuy_gateway_merchant_id'])) {
          $this->data['laybuy_gateway_merchant_id'] = $this->request->post['laybuy_gateway_merchant_id'];
      } else {
          $this->data['laybuy_gateway_merchant_id'] = $this->config->get('laybuy_gateway_merchant_id');
      }
      
      if (isset($this->request->post['laybuy_gateway_order_status_id'])) {
          $this->data['laybuy_gateway_order_status_id'] = $this->request->post['laybuy_gateway_order_status_id'];
      } else {
          $this->data['laybuy_gateway_order_status_id'] = $this->config->get('laybuy_gateway_order_status_id');
      }
      
      $this->load->model('localisation/order_status');

      $this->data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();
      
      if (isset($this->request->post['laybuy_gateway_geo_zone_id'])) {
          $this->data['laybuy_gateway_geo_zone_id'] = $this->request->post['laybuy_gateway_geo_zone_id'];
      } else {
          $this->data['laybuy_gateway_geo_zone_id'] = $this->config->get('laybuy_gateway_geo_zone_id');
      }

      if (isset($this->request->post['laybuy_gateway_total'])) {
          $this->data['laybuy_gateway_total'] = $this->request->post['laybuy_gateway_total'];
      } else {
          $this->data['laybuy_gateway_total'] = $this->config->get('laybuy_gateway_total');
      }      

      if (isset($this->request->post['laybuy_gateway_max_total'])) {
          $this->data['laybuy_gateway_max_total'] = $this->request->post['laybuy_gateway_max_total'];
      } else {
          $this->data['laybuy_gateway_max_total'] = $this->config->get('laybuy_gateway_max_total');
      }    

      if (isset($this->request->post['laybuy_gateway_tax'])) {
          $this->data['laybuy_gateway_tax'] = $this->request->post['laybuy_gateway_tax'];
      } else {
          $this->data['laybuy_gateway_tax'] = $this->config->get('laybuy_gateway_tax');
      }  

      $this->load->model('localisation/geo_zone');

      $this->data['geo_zones'] = $this->model_localisation_geo_zone->getGeoZones();

      if (isset($this->request->post['laybuy_gateway_status'])) {
          $this->data['laybuy_gateway_status'] = $this->request->post['laybuy_gateway_status'];
      } else {
          $this->data['laybuy_gateway_status'] = $this->config->get('laybuy_gateway_status');
      }
                
      if (isset($this->request->post['laybuy_gateway_debug'])) {
          $this->data['laybuy_gateway_debug'] = $this->request->post['laybuy_gateway_debug'];
      } else {
          $this->data['laybuy_gateway_debug'] = $this->config->get('laybuy_gateway_debug');
      }
      
      if (isset($this->request->post['laybuy_gateway_sort_order'])) {
          $this->data['laybuy_gateway_sort_order'] = $this->request->post['laybuy_gateway_sort_order'];
      } else {
          $this->data['laybuy_gateway_sort_order'] = $this->config->get('laybuy_gateway_sort_order');
      }
						

			$this->template = 'payment/laybuy_gateway.tpl';
			$this->children = array(
							'common/header',
							'common/footer'
			);
			$this->response->setOutput($this->render());
    }

    protected function validate() {

      if (!$this->user->hasPermission('modify', 'payment/laybuy_gateway')) {
          $this->error['warning'] = $this->language->get('error_permission');
      }

			if (!$this->error) {
				return true;
			} else {
				return false;
			}	
    }
    
	public function install() {    
      //Modify DB to add laybuy token
      try {
        $this->db->query("ALTER TABLE `" . DB_PREFIX . 'order` ADD `laybuy_token` VARCHAR(100) NOT NULL');
      } catch (Exception $ex) {
        //ignore duplicate errors (already added previously)
      }
	}
    
}
?>
