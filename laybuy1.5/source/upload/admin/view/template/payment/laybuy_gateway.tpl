<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
	
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
	
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/payment.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a><a href="<?php echo $cancel; ?>" class="button"><?php echo $button_cancel; ?></a></div>
    </div>
    <div class="content">	
			<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
        <table class="form">
					
          <tr>
          <tr>
						<td><?php echo $entry_api_key; ?></td>
            <td>
              <input type="text" name="laybuy_gateway_api_key" value="<?php echo $laybuy_gateway_api_key;?>" placeholder="<?php echo $entry_api_key; ?>" id="input-api-key" class="form-control"/>
            </td>
					</tr>					
					
          <tr>
            <td><?php echo $entry_merchant_id; ?></td>
            <td>
             <input type="text" name="laybuy_gateway_merchant_id" value="<?php echo $laybuy_gateway_merchant_id;?>" placeholder="<?php echo $entry_merchant_id; ?>" id="input-merchant-id" class="form-control"/>
            </td>
          </tr>
					
          <tr>
            <td><?php echo $entry_mode; ?></td>
            <td>
              <select name="laybuy_gateway_mode" id="input-mode" class="form-control">
                <?php if ($laybuy_gateway_mode == 1) { ?>
                <option value="1" selected="selected"><?php echo $text_live; ?></option>
                <option value="0"><?php echo $text_sandbox; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_live; ?></option>
                <option value="0" selected="selected"><?php echo $text_sandbox; ?></option>
                <?php } ?>
              </select>
            </td>
          </tr>
					
          <tr>
            <td><span data-toggle="tooltip" title="<?php echo $help_debug; ?>"><?php echo $entry_debug; ?></span></td>
            <td>
              <select name="laybuy_gateway_debug" id="input-debug" class="form-control">
                <?php if ($laybuy_gateway_debug) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select>
            </td>
          </tr>
          
          <tr>
            <td><?php echo $entry_geo_zone; ?></td>
            <td>
              <select name="laybuy_gateway_geo_zone_id" id="input-geo-zone" class="form-control">
                  <option value="0"><?php echo $text_all_zones; ?></option>
                  <?php foreach ($geo_zones as $geo_zone) { ?>
                  <?php if ($geo_zone['geo_zone_id'] == $laybuy_gateway_geo_zone_id) { ?>
                  <option value="<?php echo $geo_zone['geo_zone_id']; ?>" selected="selected"><?php echo $geo_zone['name']; ?></option>
                  <?php } else { ?>
                  <option value="<?php echo $geo_zone['geo_zone_id']; ?>"><?php echo $geo_zone['name']; ?></option>
                  <?php } ?>
                  <?php } ?>
                </select>
            </td>
          </tr>
					
					<tr>
            <td><span data-toggle="tooltip" title="<?php echo $help_total; ?>"><?php echo $entry_total; ?></span></td>
            <td>
              <input type="text" name="laybuy_gateway_total" value="<?php echo $laybuy_gateway_total; ?>" placeholder="<?php echo $entry_total; ?>" id="input-total" class="form-control" />
            </td>
          </tr>
					
					<tr>
            <td><span data-toggle="tooltip" title="<?php echo $help_max_total; ?>"><?php echo $entry_max_total; ?></span></td>
            <td>
              <input type="text" name="laybuy_gateway_max_total" value="<?php echo $laybuy_gateway_max_total; ?>" placeholder="<?php echo $entry_max_total; ?>" id="input-max-total" class="form-control" />
            </td>
          </tr>
					
          <tr>
            <td><?php echo $entry_order_status; ?></td>
            <td>
              <select name="laybuy_gateway_order_status_id" id="input-order-status" class="form-control">
                  <?php foreach ($order_statuses as $order_status) { ?>
                  <?php if ($order_status['order_status_id'] == $laybuy_gateway_order_status_id) { ?>
                  <option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
                  <?php } else { ?>
                  <option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
                  <?php } ?>
                  <?php } ?>
                </select>
            </td>
          </tr>
					
          <tr>
            <td><span data-toggle="tooltip" title="<?php echo $help_tax; ?>"><?php echo $entry_tax_type; ?></span></td>
            <td>
              <select name="laybuy_gateway_tax" id="input-tax-type" class="form-control">
                <?php if ($laybuy_gateway_tax != 1) { ?>
                <option value="0" selected="selected"><?php echo $text_standard; ?></option>
                <option value="1"><?php echo $text_non_standard; ?></option>
                <?php } else { ?>
                <option value="0"><?php echo $text_standard; ?></option>
                <option value="1" selected="selected"><?php echo $text_non_standard; ?></option>
                <?php } ?>
              </select>
            </td>
          </tr>
					
          <tr>
            <td><?php echo $entry_status; ?></td>
            <td>
              <select name="laybuy_gateway_status" id="input-status" class="form-control">
                  <?php if ($laybuy_gateway_status) { ?>
                  <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                  <option value="0"><?php echo $text_disabled; ?></option>
                  <?php } else { ?>
                  <option value="1"><?php echo $text_enabled; ?></option>
                  <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                  <?php } ?>
                </select>
            </td>
          </tr>
					
          <tr>
            <td><?php echo $entry_sort_order; ?></td>
            <td>
              <input type="text" name="laybuy_gateway_sort_order" value="<?php echo $laybuy_gateway_sort_order; ?>" placeholder="<?php echo $entry_sort_order; ?>" id="input-sort-order" class="form-control" />
            </td>
          </tr>
										
        </table>
			</form>			
		</div>
	</div>
</div>
<?php echo $footer; ?>
