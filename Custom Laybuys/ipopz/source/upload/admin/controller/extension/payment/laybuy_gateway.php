<?php
class ControllerExtensionPaymentLaybuyGateway extends Controller {
	private $error = array();

	public function index() {
      $this->load->language('extension/payment/laybuy_gateway');
      $this->document->setTitle($this->language->get('heading_title'));
      $this->load->model('setting/setting');

      if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
        $this->model_setting_setting->editSetting('payment_laybuy_gateway', $this->request->post);
				
				//call install to run any updates
				$this->install();

        $this->session->data['success'] = $this->language->get('text_success');

        $this->response->redirect($this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=payment', true));
      }
      
      $data['heading_title'] = $this->language->get('heading_title');

      $data['text_edit'] = $this->language->get('text_edit');
      $data['text_enabled'] = $this->language->get('text_enabled');
      $data['text_disabled'] = $this->language->get('text_disabled');
      $data['text_all_zones'] = $this->language->get('text_all_zones');

      $data['text_sandbox'] = $this->language->get('text_sandbox');
      $data['text_live'] = $this->language->get('text_live');
      
      $data['text_standard'] = $this->language->get('text_standard');
      $data['text_non_standard'] = $this->language->get('text_non_standard');

      $data['entry_api_key'] = $this->language->get('entry_api_key');
      $data['entry_merchant_id'] = $this->language->get('entry_merchant_id');
      $data['entry_mode'] = $this->language->get('entry_mode');
      $data['entry_debug'] = $this->language->get('entry_debug');
      $data['entry_total'] = $this->language->get('entry_total');
      $data['entry_order_status'] = $this->language->get('entry_order_status');
      $data['entry_total'] = $this->language->get('entry_total');
      $data['entry_max_total'] = $this->language->get('entry_max_total');
      $data['entry_tax_type'] = $this->language->get('entry_tax_type');
      $data['entry_geo_zone'] = $this->language->get('entry_geo_zone');        
      $data['entry_status'] = $this->language->get('entry_status');
      $data['entry_sort_order'] = $this->language->get('entry_sort_order');

      $data['help_total'] = $this->language->get('help_total');
      $data['help_max_total'] = $this->language->get('help_max_total');
      $data['help_debug'] = $this->language->get('help_debug');
      $data['help_tax'] = $this->language->get('help_tax');

      $data['button_save'] = $this->language->get('button_save');
      $data['button_cancel'] = $this->language->get('button_cancel');

      if (isset($this->error['warning'])) {
          $data['error_warning'] = $this->error['warning'];
      } else {
          $data['error_warning'] = '';
      }

      $data['breadcrumbs'] = array();

      $data['breadcrumbs'][] = array(
          'text' => $this->language->get('text_home'),
          'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
      );

      $data['breadcrumbs'][] = array(
          'text' => $this->language->get('text_extension'),
          'href' => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=payment', true)
      );

      $data['breadcrumbs'][] = array(
          'text' => $this->language->get('heading_title'),
          'href' => $this->url->link('extension/payment/laybuy_gateway', 'user_token=' . $this->session->data['user_token'], true)
      );

      $data['action'] = $this->url->link('extension/payment/laybuy_gateway', 'user_token=' . $this->session->data['user_token'], true);

      $data['cancel'] = $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=payment', true);


      if (isset($this->request->post['payment_laybuy_gateway_mode'])) {
       $data['payment_laybuy_gateway_mode'] = $this->request->post['payment_laybuy_gateway_mode'];
      } else {
       $data['payment_laybuy_gateway_mode'] = $this->config->get('payment_laybuy_gateway_mode');
      }      
            
      if (isset($this->request->post['payment_laybuy_gateway_api_key'])) {
          $data['payment_laybuy_gateway_api_key'] = $this->request->post['payment_laybuy_gateway_api_key'];
      } else {
          $data['payment_laybuy_gateway_api_key'] = $this->config->get('payment_laybuy_gateway_api_key');
      }
      
      if (isset($this->request->post['payment_laybuy_gateway_merchant_id'])) {
          $data['payment_laybuy_gateway_merchant_id'] = $this->request->post['payment_laybuy_gateway_merchant_id'];
      } else {
          $data['payment_laybuy_gateway_merchant_id'] = $this->config->get('payment_laybuy_gateway_merchant_id');
      }
      
      if (isset($this->request->post['payment_laybuy_gateway_order_status_id'])) {
          $data['payment_laybuy_gateway_order_status_id'] = $this->request->post['payment_laybuy_gateway_order_status_id'];
      } else {
          $data['payment_laybuy_gateway_order_status_id'] = $this->config->get('payment_laybuy_gateway_order_status_id');
      }
      
      $this->load->model('localisation/order_status');

      $data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();
      
      if (isset($this->request->post['payment_laybuy_gateway_geo_zone_id'])) {
          $data['payment_laybuy_gateway_geo_zone_id'] = $this->request->post['payment_laybuy_gateway_geo_zone_id'];
      } else {
          $data['payment_laybuy_gateway_geo_zone_id'] = $this->config->get('payment_laybuy_gateway_geo_zone_id');
      }

      if (isset($this->request->post['payment_laybuy_gateway_total'])) {
          $data['payment_laybuy_gateway_total'] = $this->request->post['payment_laybuy_gateway_total'];
      } else {
          $data['payment_laybuy_gateway_total'] = $this->config->get('payment_laybuy_gateway_total');
      }     

      if (isset($this->request->post['payment_laybuy_gateway_max_total'])) {
          $data['payment_laybuy_gateway_max_total'] = $this->request->post['payment_laybuy_gateway_max_total'];
      } else {
          $data['payment_laybuy_gateway_max_total'] = $this->config->get('payment_laybuy_gateway_max_total');
      }     

      if (isset($this->request->post['payment_laybuy_gateway_tax'])) {
          $data['payment_laybuy_gateway_tax'] = $this->request->post['payment_laybuy_gateway_tax'];
      } else {
          $data['payment_laybuy_gateway_tax'] = $this->config->get('payment_laybuy_gateway_tax');
      }  

      $this->load->model('localisation/geo_zone');

      $data['geo_zones'] = $this->model_localisation_geo_zone->getGeoZones();

      if (isset($this->request->post['payment_laybuy_gateway_status'])) {
          $data['payment_laybuy_gateway_status'] = $this->request->post['payment_laybuy_gateway_status'];
      } else {
          $data['payment_laybuy_gateway_status'] = $this->config->get('payment_laybuy_gateway_status');
      }
                
      if (isset($this->request->post['payment_laybuy_gateway_debug'])) {
          $data['payment_laybuy_gateway_debug'] = $this->request->post['payment_laybuy_gateway_debug'];
      } else {
          $data['payment_laybuy_gateway_debug'] = $this->config->get('payment_laybuy_gateway_debug');
      }
      
      if (isset($this->request->post['payment_laybuy_gateway_sort_order'])) {
          $data['payment_laybuy_gateway_sort_order'] = $this->request->post['payment_laybuy_gateway_sort_order'];
      } else {
          $data['payment_laybuy_gateway_sort_order'] = $this->config->get('payment_laybuy_gateway_sort_order');
      }

      $data['header'] = $this->load->controller('common/header');
      $data['column_left'] = $this->load->controller('common/column_left');
      $data['footer'] = $this->load->controller('common/footer');

      $this->response->setOutput($this->load->view('extension/payment/laybuy_gateway', $data));
	}

    protected function validate() {
 
      if (!$this->user->hasPermission('modify', 'extension/payment/laybuy_gateway')) {
          $this->error['warning'] = $this->language->get('error_permission');
      }

      return !$this->error;
    }
    
	public function install() {    
      $this->load->model('setting/event');
			
			if (!$this->model_setting_event->getEventByCode('laybuy_product_info')){
				$this->model_setting_event->addEvent('laybuy_product_info', 'catalog/view/product/product/before', 'extension/payment/laybuy_gateway/productInfo');
			}
			if (!$this->model_setting_event->getEventByCode('laybuy_category_info')){
				$this->model_setting_event->addEvent('laybuy_category_info', 'catalog/view/product/category/before', 'extension/payment/laybuy_gateway/categoryInfo');
			}
			if (!$this->model_setting_event->getEventByCode('laybuy_checkout_info')){
				$this->model_setting_event->addEvent('laybuy_checkout_info', 'catalog/view/checkout/checkout/before', 'extension/payment/laybuy_gateway/checkoutInfo');
			}
			
      
      //Modify DB to add laybuy user_token
      try {
        $this->db->query("ALTER TABLE `" . DB_PREFIX . 'order` ADD `laybuy_token` VARCHAR(100) NOT NULL');
      } catch (Exception $ex) {
        //ignore duplicate errors (already added previously)
      }
      
	}
    
	public function uninstall() {
		// delete the event triggers
      $this->load->model('setting/event');
      $this->model_setting_event->deleteEventByCode('laybuy_product_info');
      $this->model_setting_event->deleteEventByCode('laybuy_category_info');
      $this->model_setting_event->deleteEventByCode('laybuy_checkout_info');
	}
}
?>
