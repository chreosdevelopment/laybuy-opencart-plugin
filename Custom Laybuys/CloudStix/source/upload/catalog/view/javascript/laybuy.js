if (typeof Laybuy === 'undefined') {
	var Laybuy = {};
	Laybuy.loadScript = function(url, callback) {
		var script = document.createElement('script');
		script.type = 'text/javascript';
		if (script.readyState) { // I.E.
			script.onreadystatechange = function() {
				if (script.readyState == 'loaded' || script.readyState == 'complete') {
					script.onreadystatechange = null;
					callback();
				}
			};
		} else { // Others
			script.onload = function() {
				callback();
			};
		}
		script.src = url;
		document.getElementsByTagName('head')[0].appendChild(script);
	};

	Laybuy.launchPopup = function($, event) {
		event.preventDefault();

		var $popup_wrapper, $popup_outer, $popup_inner, $a, $img, $close_button, $currency;
	    $popup_wrapper = $('#laybuy-popup-wrapper');

	    if ($popup_wrapper.length > 0) {
	    	$popup_wrapper.show();
		} else {
	    	$popup_wrapper = $(document.createElement('div'))
	            .attr('id', 'laybuy-popup-wrapper')
	            .css({
	              	'position': 'fixed',
	              	'z-index': 999999999,
	              	'left': 0,
	              	'top': 0,
	              	'right': 0,
	              	'bottom': 0,
	              	'overflow': 'auto'
	            })
	            .appendTo('body')
	            .on('click', function(event) {
	              	Laybuy.closePopup($, event);
	            });
							

			$popup_outer = $(document.createElement('div'))
	        	.attr('id', 'laybuy-popup-outer')
	            .css({
	              	'display': '-ms-flexbox',
	              	'display': '-webkit-flex',
	              	'display': 'flex',
	              	'-webkit-justify-content': 'center',
	              	'-ms-flex-pack': 'center',
	              	'justify-content': 'center',
	              	'-webkit-align-content': 'center',
	              	'-ms-flex-line-pack': 'center',
	              	'align-content': 'center',
	              	'-webkit-align-items': 'center',
	              	'-ms-flex-align': 'center',
	              	'align-items': 'center',
	              	'width': '100%',
	              	'min-height': '100%',
	              	'background-color': 'rgba(0, 0, 0, 0.80)'
	            })
	            .appendTo($popup_wrapper);

			$popup_inner = $(document.createElement('div'))
	        	.attr('id', 'laybuy-popup-inner')
	            .css({
	            	'position': 'relative',
	              'width': '400px',
	              'height': '650px'
	            })
							.html('<iframe id="laybuy-popup" frameborder="0" src="https://popup.laybuy.com/" style="width: 100%; height: 100%;"></iframe>')
	            .appendTo($popup_outer);


			$close_button = $(document.createElement('a'))
	        	.attr('href', '#')
	            .css({
	            	'position': 'absolute',
	              	'right': '8px',
	              	'top': '8px'
	            })
	            .html('<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 32 32" version="1.1" width="32px" height="32px"><g id="surface1"><path style=" " d="M 16 3 C 8.832031 3 3 8.832031 3 16 C 3 23.167969 8.832031 29 16 29 C 23.167969 29 29 23.167969 29 16 C 29 8.832031 23.167969 3 16 3 Z M 16 5 C 22.085938 5 27 9.914063 27 16 C 27 22.085938 22.085938 27 16 27 C 9.914063 27 5 22.085938 5 16 C 5 9.914063 9.914063 5 16 5 Z M 12.21875 10.78125 L 10.78125 12.21875 L 14.5625 16 L 10.78125 19.78125 L 12.21875 21.21875 L 16 17.4375 L 19.78125 21.21875 L 21.21875 19.78125 L 17.4375 16 L 21.21875 12.21875 L 19.78125 10.78125 L 16 14.5625 Z "/></g></svg>')
	            .appendTo($popup_inner)
	            .on('click', function(event) {
	              	Laybuy.closePopup($, event);
	            });
		}
	};
	Laybuy.closePopup = function($, event) {
	    event.preventDefault();
		$('#laybuy-popup-wrapper').hide();
	};

	Laybuy.init = function($) {

		$(document).on("click", "a[href='https://popup.laybuy.com']", function(event){
          	Laybuy.launchPopup($, event);
        });
	};
	if (typeof jQuery === 'undefined' || parseFloat(jQuery.fn.jquery) < 1.7) {
        Laybuy.loadScript('https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js', function() {
          	var jQuery_1_12_4 = jQuery.noConflict(true);
          	Laybuy.init(jQuery_1_12_4);
        });
    } else {
        	Laybuy.init(jQuery);
    }
}

