<?php
class ControllerExtensionPaymentLaybuyGateway extends Controller {
  
  var $aud_limit      = 1440;
  var $nzd_limit      = 1440;
  var $default_limit  = 720;
  
  public function __construct($registry) {
    parent::__construct($registry);

    require_once(modification(DIR_SYSTEM . 'library/laybuy.php'));
    $this->laybuy = new Laybuy($this->registry);   
  }
  
  
  public function index() {
    $this->load->language('extension/payment/laybuy_gateway');
    
    $data = array();
    $data['text_heading'] = $this->language->get('text_heading');
    $data['text_instruction'] = $this->language->get('text_instruction');
    $data['text_loading'] = $this->language->get('text_loading');
    $data['button_confirm'] = $this->language->get('button_confirm');
    
    //--- formatting for price breakdown    
    $order_id = $this->session->data['order_id'];  
    $this->load->model('checkout/order');
    $order_info = $this->model_checkout_order->getOrder($order_id);
    
    //Get limit based on currency or get default
    $var = strtolower($this->config->get('config_currency'))."_limit";
    $limit = isset($this->$var)? $this->$var : $this->default_limit;
    
    if ($order_info['total'] > $limit) {
      $repayment = floor($limit / 6 * 100)/100;
      $excess = $this->currency->format($order_info['total'] - (5*($limit / 6)), $order_info['currency_code']);
      $repayment = $this->currency->format($repayment, $order_info['currency_code'], $order_info['currency_value']);
      $data['text_payment'] = sprintf($this->language->get('text_payment_excess'), $excess, $repayment);
    } else {
      $repayment = $this->currency->format(floor($order_info['total'] / 6 * 100)/100, $order_info['currency_code']);
      $data['text_payment'] = sprintf($this->language->get('text_payment'), $repayment);
    }
    //----

	return $this->load->view('extension/payment/laybuy_gateway', $data);
  }

  
  public function productInfo(&$route, &$data, &$output){
    if ($data['price'] && !empty($this->request->get['product_id']) && $this->config->get('laybuy_gateway_status')) {
      $this->load->language('extension/payment/laybuy_gateway');

      $this->load->model('catalog/product');

      $product_info = $this->model_catalog_product->getProduct($this->request->get['product_id']);

      //--- formatting for price breakdown  			
			$language_prefix = '';
			if ($this->config->get('config_country_id') == 153 || $this->config->get('config_country_id') == 13) {
				$language_prefix = 'anz_';
			}	
			
      $var = strtolower($this->config->get('config_currency'))."_limit";
      
      $price = $this->tax->calculate(($data['special'] ? $product_info['special']  : $product_info['price']), $product_info['tax_class_id'], $this->config->get('config_tax'));

  
        
      if ($price > $this->config->get('laybuy_gateway_total') && (!$this->config->get('laybuy_gateway_max_total') || $this->config->get('laybuy_gateway_max_total') > $price)) {

        $limit = isset($this->$var)? $this->$var : $this->default_limit;
             
        if ($price > $limit) {
          $repayment = floor($limit / 6 * 100)/100;          
          $excess = $this->currency->format($price - (5*($limit / 6)), $this->session->data['currency']);
          $repayment = $this->currency->format($repayment, $this->session->data['currency']);
          $text_payment = addslashes(sprintf($this->language->get('text_'.$language_prefix.'info_payment_excess'), $excess, $repayment));
        } else {
          $repayment = $this->currency->format(floor($price / 6 * 100)/100, $this->session->data['currency']);
          $text_payment = addslashes(sprintf($this->language->get('text_'.$language_prefix.'info_payment'), $repayment));
        }
        //----
				
				$data['laybuy_info'] = $text_payment ? $text_payment : "";
        $data['footer'] = "<script type='text/javascript'><!--\n$(document).ready(function() { $('#product').before('\\n{$text_payment}\\n')});\n//--></script>\n{$data['footer']}";    
      }
    }
  }
  

  public function categoryInfo(&$route, &$data, &$output){
    $this->load->language('extension/payment/laybuy_gateway');
    $this->load->model('catalog/product');    
    
    foreach ($data['products'] as $key => $product) {
      $text_payment = false;
      if ($product['price'] && $this->config->get('laybuy_gateway_status')) {
        
        $product_info = $this->model_catalog_product->getProduct($product['product_id']);
        
        $var = strtolower($this->config->get('config_currency'))."_limit";
        
        $price = $this->tax->calculate(($product['special'] ? $product_info['special']  : $product_info['price']), $product_info['tax_class_id'], $this->config->get('config_tax'));
        
				
        if ($price > $this->config->get('payment_laybuy_gateway_total') && (!$this->config->get('payment_laybuy_gateway_max_total') || $this->config->get('payment_laybuy_gateway_max_total') > $price)) {

          $limit = isset($this->$var)? $this->$var : $this->default_limit;

          if ($price > $limit) {
            $repayment = floor($limit / 6 * 100)/100;          
            $excess = $this->currency->format($price - (5*($limit / 6)), $this->session->data['currency']);
            $repayment = $this->currency->format($repayment, $this->session->data['currency']);
            $text_payment = sprintf($this->language->get('text_category_payment_excess'), $excess, $repayment);
          } else {
            $repayment = $this->currency->format(floor($price / 6 * 100)/100, $this->session->data['currency']);
            $text_payment = sprintf($this->language->get('text_category_payment'), $repayment);
          }
          //----
              
        }
      }
      $data['products'][$key]['laybuy_info'] = $text_payment ? $text_payment : "";
    }
    
  }
  
  
  public function send() {    
    $this->load->language('extension/payment/laybuy_gateway');
    $result = false; 

    if($this->config->get('laybuy_gateway_status')) {
      $this->load->model('checkout/order');
      $this->load->model('account/order');

      $order_id = $this->session->data['order_id'];    
      $order_info = $this->model_checkout_order->getOrder($order_id); 
      
      if ($order_info) {
        $data = array(
          "amount" => $order_info['total'],
          "currency" => $order_info['currency_code'],
//          "returnUrl" => $this->url->link('laybuy_gateway', '', true),
          "returnUrl" => $this->url->link('extension/payment/laybuy_gateway/confirm', '', true),
          "merchantReference" => $order_info['order_id']
        );

        $data['customer'] = array(
          "firstName" => $order_info["firstname"],
          "lastName" => $order_info["lastname"],
          "email" => $order_info["email"],
          "phone" => $order_info["telephone"]
        );

        $data['billingAddress'] = array(
          "name" => "{$order_info['payment_firstname']} {$order_info['payment_lastname']}",
          "address1" => $order_info["payment_address_1"],
          "address2" => $order_info["payment_address_2"],          
          "city" => $order_info["payment_city"],
          "state" => $order_info["payment_zone"],
          "postcode" => $order_info["payment_postcode"],
          "country" => $order_info["payment_country"],
        );

        $data['shippingAddress'] = array(
          "name" => "{$order_info['shipping_firstname']} {$order_info['shipping_lastname']}",
          "address1" => $order_info["shipping_address_1"],
          "address2" => $order_info["shipping_address_2"],
          "city" => $order_info["shipping_city"],
          "state" => $order_info["shipping_zone"],
          "postcode" => $order_info["shipping_postcode"],
          "country" => $order_info["shipping_country"],
        );

        $products = $this->model_account_order->getOrderProducts($order_id);
        $order_tax = 0;
        $item_total = 0;
        foreach ($products as $product) {
          $item_total += ($product['price'] + $product['tax']) * $product['quantity'];
          $order_tax += $product['tax'] * $product['quantity'];
          
          $data['items'][] = array(
//            "id" => $product['model'],
            "id" => $product['order_product_id'],
            "description" => $product['name'],
            "quantity" => $product['quantity'],
            "price" => $product['price'] + $product['tax']     
          );        
        }
        
        $shipping = isset($this->session->data['shipping_method'])? $this->session->data['shipping_method']['cost'] : 0;
        if (!empty($shipping) && $this->session->data['shipping_method']['tax_class_id']) {
            $tax_rates = $this->tax->getRates($shipping, $this->session->data['shipping_method']['tax_class_id']);

            foreach ($tax_rates as $tax_rate) {
                $order_tax += $tax_rate['amount'];
                $shipping += $tax_rate['amount'];
            }
        }
        
        if (!empty($shipping)) {
          //Add any remainder as assumed tax to the shipping                  
          $data['items'][] = array(
              "id" => "SHIPPING",
              "description" => $order_info['shipping_method'],
              "quantity" => 1,
              "price" =>  $shipping  
          ); 
          $item_total += $shipping;
        }
        
        $other = $order_info['total'] - $item_total;
        
        //Discount on order accounting for rounding adjustment
        if (!empty($other) && $other < -0.01) {
          $data['items'][] = array(
              "id" => "DISCOUNT",
              "description" => $this->language->get('text_discount'),
              "quantity" => 1,
              "price" => $other   
          ); 
        }
        
        //Other costs and adjustments on order accounting for rounding adjustment
        if (!empty($other) && $other > -0.01) {
          $data['items'][] = array(
              "id" => "OTHER",
              "description" => $this->language->get('text_other_cost'),
              "quantity" => 1,
              "price" => $other   
          ); 
        }

        //DEBUG
        //$data['other'] = $other;
        //$data['item_total'] = $item_total;
        
        
        //ONLY provide tax if enabled
        if ($this->config->get('laybuy_gateway_tax')) {
          $data['tax'] = $order_tax;
        }
        
        //DEBUG
        //$this->log->write($data);
        
        $result = $this->laybuy->createOrder($data);
      }
    }
    
    if(!empty($result)) {      
      switch ($result['result']) {
        case 'SUCCESS':
          $this->load->model('extension/payment/laybuy_gateway');
          $this->model_extension_payment_laybuy_gateway->setLaybuyToken($order_id, $result['token']);
          echo $result['paymentUrl'];
          break;
        case 'ERROR':
        default:
          $this->session->data['error'] = !empty($result['error'])? $result['error'] : $this->language->get('error_laybuy');          
          echo $this->url->link('checkout/checkout');
          break;
      }
    } else {
      $this->session->data['error'] = $this->language->get('error_laybuy');
      echo $this->url->link('checkout/checkout');
    }
  }
  
  
  public function confirm($laybuy_token = false) {    
    $result = false;
    
    $token = !empty($this->request->get['token'])? $this->request->get['token'] : $laybuy_token;
    
    $this->load->model('extension/payment/laybuy_gateway');
    $order_id = $token? $this->model_extension_payment_laybuy_gateway->getOrderId($token) : false;
     
    if($order_id) {
      $result =  $this->laybuy->confirmOrder($token);
    }    
    
    if ($result !== false){
      switch ($result['result']) {
        case 'SUCCESS':
          $this->load->model('checkout/order');                    
          $this->model_checkout_order->addOrderHistory($order_id, $this->config->get('laybuy_gateway_order_status_id'));

          if ($laybuy_token) return 'abandoned transaction found and processed'; //cron call      
          $this->response->redirect($this->url->link('checkout/success', '', true));
          break;
        case 'ERROR':
        case 'DECLINED':
        default:
          $this->session->data['error'] = !empty($result['error'])? $result['error'] : $this->language->get('error_laybuy_confirm'); 
          if ($laybuy_token) return $this->session->data['error']; //cron call  
          $this->response->redirect($this->url->link('checkout/checkout', '', true));
          break;
      }
    } else {
      if ($laybuy_token) return 'connection issue see log'; //cron call  
      $this->response->redirect($this->url->link('checkout/checkout', '', true));
    }
  }
  
  
  public function cron() { 
    $this->load->model('extension/payment/laybuy_gateway');
    $orders = $this->model_extension_payment_laybuy_gateway->getOrders();
    
    foreach ($orders as $order) {
      echo "Checking: {$order['laybuy_token']} ---- ";
      $result = $this->confirm($order['laybuy_token']);
      echo "Result: \"{$result}\"<br>";
    }
    
    echo "COMPLETE";
  }
 
}
?>
