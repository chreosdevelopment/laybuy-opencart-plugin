<div class="well well-sm">
  <div id="laybuy_logo"><?php echo $text_heading; ?></div>
  <div><?php echo $text_payment; ?></div>
  <div><?php echo $text_instruction; ?></div>
</div>
<div class="buttons">
  <div class="pull-right">
     <input type="button" value="<?php echo $button_confirm; ?>" id="button-confirm" class="btn btn-primary" data-loading-text="<?php echo $text_loading; ?>" />
  </div>
</div>
<script type="text/javascript"><!--
$('#button-confirm').bind('click', function() {
	$.ajax({
		type: 'post',
		url: 'index.php?route=extension/payment/laybuy_gateway/send',
        dataType: 'text',
		beforeSend: function() {
			$('#button-confirm').button('loading');
		},
		success: function(response) {
          location = response;
		}
	});
});
//--></script>
