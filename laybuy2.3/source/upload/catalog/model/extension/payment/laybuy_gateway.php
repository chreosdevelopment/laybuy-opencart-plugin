<?php
class ModelExtensionPaymentLaybuyGateway extends Model {

  public function getOrderId($laybuy_token) {
    $query = $this->db->query("SELECT `order_id` FROM `" . DB_PREFIX . "order` WHERE `laybuy_token` = '" . $this->db->escape($laybuy_token) . "'");
    return !empty($query->row['order_id'])? $query->row['order_id'] : false;
  }  
  
  public function getOrders() {
    $query = $this->db->query("SELECT `laybuy_token` FROM `" . DB_PREFIX . "order` WHERE `laybuy_token` <> '' AND `order_status_id` = 0 AND TIMESTAMPDIFF(HOUR ,date_added,now()) = 1");
    return empty($query->row['laybuy_token'])? array() : $query->rows;
  }
  
  public function getLaybuyToken($order_id) {
    $query = $this->db->query("SELECT `laybuy_token` FROM `" . DB_PREFIX . "order` WHERE `order_id` = '" . (int)$order_id . "'"); 
    return !empty($query->row['laybuy_token'])? $query->row['laybuy_token'] : false;
  }
  
  public function setLaybuyToken($order_id, $laybuy_token) {
    $this->db->query("UPDATE `" . DB_PREFIX . "order` SET `laybuy_token` = '".$this->db->escape($laybuy_token)."' WHERE `order_id` = ".(int)$order_id);
  }
  
  public function getMethod($address, $total) {
      $this->load->language('extension/payment/laybuy_gateway');

      $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone WHERE geo_zone_id = '" . (int)$this->config->get('laybuy_gateway_geo_zone_id') . "' AND country_id = '" . (int)$address['country_id'] . "' AND (zone_id = '" . (int)$address['zone_id'] . "' OR zone_id = '0')");

      if ($this->config->get('laybuy_gateway_total') > 0 && $this->config->get('laybuy_gateway_total') > $total) {
          $status = false; 
      } elseif ($this->config->get('laybuy_gateway_max_total') > 0 && $this->config->get('laybuy_gateway_max_total') < $total) {
          $status = false;
      } elseif (!$this->config->get('laybuy_gateway_geo_zone_id')) {
          $status = true;
      } elseif ($query->num_rows) {
          $status = true;
      } else {
          $status = false;
      }

			$status = true;
			
      $method_data = array();
      if ($status) {
          $method_data = array(
              'code'       => 'laybuy_gateway',
              'title'      => $this->language->get('text_title'),
              'terms'      => '',
              'sort_order' => $this->config->get('laybuy_gateway_sort_order')
          );
      }

      return $method_data;
  }
}
?>
