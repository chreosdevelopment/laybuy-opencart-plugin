OpenCart LayBuy Payment Extension


This repository contains the latest versions of the Laybuy Extension. 

To download the extension and find instuctions on how to install it within OpenCart:

- Find the folder that matches your version of Opencart and download the zip file inside of it. 
		Note: that the version 2.x will not work with versions 2.3 of OpenCart
- To download, click the zip file and then click "View raw".
- Within the Zip file you will find a readme file with instructions relating to your version of Opencart. 



This extension requires a merchant account with Laybuy. 
Use this link to find out about becoming a merchant: 
www.laybuy.com/nz/our-merchants


If you need help contact us:
help.laybuy.com/hc/en-us/requests/new




Other Links:
Laybuy: www.laybuy.com
Facebook: www.facebook.com/PaybyLaybuy/
The gram: www.instagram.com/paybylaybuy/
Twitter: twitter.com/PaybyLaybuy





Also a shameless plug to us the developers...

Laybuy contracted us to make this plugin for them. We can also provide you with any 
web development, app or software development project you may have.
Contact us here mascotweb.nz/contact