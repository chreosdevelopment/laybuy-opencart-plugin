<?php
// Text
$_['text_title'] = "Pay By Laybuy";
$_['text_heading'] = '<h3>Pay by Laybuy</h3>';
$_['text_payment'] = "<div style='font-weight: 600; font-size: 12px; line-height: 35px; margin-bottom: 10px; color: #666;'>6 interest-free payments of <strong style='color:#786DFF;'>%s</strong><a href='https://popup.laybuy.com' target='_blank' title='Laybuy' style='color: #666'><img src='image/catalog/laybuy_logo.svg' alt='Laybuy' style='width: 83px; vertical-align: top;' />  <small style='text-decoration: underline;'>Learn more</small></a></div>";
$_['text_payment_excess']  = "You will only pay From %s today & 5 weekly interest free payments from <strong style='color:#786DFF;'>%s</strong><br>";
$_['text_discount']  = "Discount on order";
$_['text_other_cost']  = "Other costs related to sale";


$_['text_instruction']  = "<p>Laybuy lets you receive your purchase now and spread the total cost over 6 weekly automatic payments.</p><p>When you click 'confirm order' you will be redirected to Laybuy to complete your order.</p>";

//HTML for product info page
$_['text_info_payment'] = "<div style='line-height: 35px; font-size: 11px; font-weight: 600; color: #666;' id='laybuy_gatway_product_info'>or 6 interest-free payments of %s <a href='https://popup.laybuy.com' target='_blank' title='Laybuy' style='color: #666'><img src='image/catalog/laybuy_logo.svg' alt='Laybuy' style='width: 83px;vertical-align: top;'  /> <small style='text-decoration: underline;'>Learn more</small></a></div><br>";
$_['text_info_payment_excess']  = "<div style='line-height: 35px; font-size: 11px; font-weight: 600; color: #666;' id='laybuy_gatway_product_info'>or %s today & 5 interest-free payments of %s <a href='https://popup.laybuy.com' target='_blank' title='Laybuy' style='color: #666'><img src='image/catalog/laybuy_logo.svg' alt='Laybuy' style='width: 83px; vertical-align: top;' />  <small style='text-decoration: underline;'>Learn more</small></a></div><br>";

//HTML for product info page ANZ
$_['text_anz_info_payment'] = "<div style='line-height: 35px; font-size: 11px; font-weight: 600; color: #666;' id='laybuy_gatway_product_info'>or 6 interest-free payments of %s <a href='https://popup.laybuy.com' target='_blank' title='Laybuy' style='color: #666'><img src='image/catalog/laybuy_logo.svg' alt='Laybuy' style='width: 83px; vertical-align: top;'  /> <small style='text-decoration: underline;'>Learn more</small></a></div><br>";
$_['text_anz_info_payment_excess']  = "<div style='line-height: 35px; font-size: 11px; font-weight: 600; color: #666;' id='laybuy_gatway_product_info'>or %s today & 5 interest-free payments of %s <a href='https://popup.laybuy.com' target='_blank' title='Laybuy' style='color: #666'><img src='image/catalog/laybuy_logo.svg' alt='Laybuy' style='width: 83px; vertical-align: top;' />  <small style='text-decoration: underline;'>Learn more</small></a></div><br>";


//HTML for category listing page
$_['text_category_payment'] = "<div style='line-height: 27px; font-size: 11px; font-weight: 600; color: #666;' id='laybuy_gatway_product_info'>or 6 payments of %s<a href='https://popup.laybuy.com' target='_blank' title='Laybuy' style='color: #666'><img src='image/catalog/laybuy_logo.svg' alt='Laybuy' style='width: 70px; vertical-align: top;' /></a></div><br>";
$_['text_category_payment_excess']  = "<div style='line-height: 27px; font-size: 11px; font-weight: 600; color: #666;' id='laybuy_gatway_product_info'>or %s today & 5 payments of %s<a href='https://popup.laybuy.com' target='_blank' title='Laybuy' style='color: #666'><img src='image/catalog/laybuy_logo.svg' alt='Laybuy' style='width: 70px; vertical-align: top;' /></a></div><br>";

//HTML for category listing page ANZ
$_['text_anz_category_payment'] = "<div style='line-height: 27px; font-size: 11px; font-weight: 600; color: #666;' id='laybuy_gatway_product_info'>or 6 payments of %s<a href='https://popup.laybuy.com' target='_blank' title='Laybuy' style='color: #666'><img src='image/catalog/laybuy_logo.svg' alt='Laybuy' style='width: 70px; vertical-align: top;'  /></a></div><br>";
$_['text_anz_category_payment_excess']  = "<div style='line-height: 27px; font-size: 11px; font-weight: 600; color: #666;' id='laybuy_gatway_product_info'>or %s today & 5 payments from %s<a href='https://popup.laybuy.com' target='_blank' title='Laybuy' style='color: #666'><img src='image/catalog/laybuy_logo.svg' alt='Laybuy' style='width: 70px; vertical-align: top;' /></a></div><br>";


$_['error_laybuy']  = "Laybuy payment currently unavailable! Please try again later.";
$_['error_laybuy_confirm']  = "Laybuy payment currently unavailable! If payment was made please wait 4 hours to see if your order completes otherwise <a href='index.php?route=information/contact'>contact us</a>.";
?>