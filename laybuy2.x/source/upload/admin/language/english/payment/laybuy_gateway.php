<?php
// Heading
$_['heading_title']       = 'Laybuy Payment Gateway';

//Logo
$_['text_laybuy_gateway'] = '<a href="https://www.laybuy.com/" target="_blank"><img src="view/image/payment/laybuy.png" alt="Laybuy" title="Laybuy" style="border: 1px solid #EEEEEE;max-width: 100px;padding:2px" /></a>';

// Text
$_['text_payment']	      = 'Payment';
//$_['text_extension']      = 'Extensions';
$_['text_success']        = 'Success: You have modified Laybuy payment details!';
$_['text_edit']           = 'Edit Laybuy';
$_['text_sandbox']        = 'Sandbox';
$_['text_live']           = 'Live';
$_['text_standard']       = 'Standard';
$_['text_non_standard']   = 'Non Standard';
$_['button_confirm']			= 'Confirm Order';

// Entry
$_['entry_api_key']       = 'API Key';
$_['entry_merchant_id']   = 'Merchant ID';
$_['entry_mode']          = 'Mode';
$_['entry_debug']         = 'Debug logging';
$_['entry_total']         = 'Total';
$_['entry_max_total']     = 'Max Total';
$_['entry_tax_type']      = 'Tax Type';
$_['entry_order_status']  = 'Order Status';
$_['entry_geo_zone']      = 'Geo Zone';
$_['entry_status']        = 'Status';
$_['entry_sort_order']    = 'Sort Order';

// Help
$_['help_total']          = 'The checkout total the order must reach before this payment method becomes active.';
$_['help_max_total']      = 'The maximum checkout/item total the payment method will be available for (leave blank for no limit).';
$_['help_debug']          = 'Enabling debug will write data to a log file that can be used to track issues. You should always disable unless instructed otherwise';
$_['help_tax']			  = 'Non Standard is for merchants who do not use the default tax rate and are from the following countries: United Kingdom. You should leave as standard unless instructed otherwise';

// Error
$_['error_permission']    = 'Warning: You do not have permission to modify payment Laybuy!';
